# Сумма квадратов первых десяти натуральных чисел равна 385
# Квадрат суммы первых десяти натуральных чисел равен 3025
# Следовательно, разность между суммой квадратов и квадратом
# суммы первых десяти натуральных чисел составляет 3025 − 385 = 2640.
#
# Найдите разность между суммой квадратов и квадратом суммы первых
# ста натуральных чисел.
#
# n*(n+1)*(2*n + 1)/6 - формула суммы квадратов
# (n*(n+1)/2)**2 - формула квадрата суммы
# Я сделал без них


import tkinter as tk


def difference():
    global square_of_sum, sum_of_squares
    sum_of_squares = 0
    square_of_sum = 0
    if ent_max_num.get() != '' and ent_max_num.get().isdigit():
        for num in range(1, int(ent_max_num.get()) + 1):
            sum_of_squares += num ** 2
            square_of_sum += num
        square_of_sum **= 2
        lbl_square_of_sum['text'] = f"Квадрат суммы\n{square_of_sum}"
        lbl_sum_of_squares['text'] = f"Сумма квадратов\n{sum_of_squares}"
        lbl_result['text'] = f"{square_of_sum} - {sum_of_squares} = {square_of_sum - sum_of_squares}"


window = tk.Tk()
window.title('Разность')
window.geometry('400x275+600+150')
window.resizable(False, False)

max_num = tk.IntVar()
max_num.set(10)

sum_of_squares = 0
square_of_sum = 0

lbl_max_num = tk.Label(window, text='Введите до какого числа считать:', font="14")
lbl_max_num.pack(side="top", pady=5)

ent_max_num = tk.Entry(width=10, font="14")
ent_max_num.pack(side="top", pady=5)

lbl_sum_of_squares = tk.Label(text=f"Сумма квадратов\n{sum_of_squares}", font="14")
lbl_sum_of_squares.place(x=210, y=100)

lbl_square_of_sum = tk.Label(text=f"Квадрат суммы\n{square_of_sum}", font="14")
lbl_square_of_sum.place(x=10, y=100)

btn_result = tk.Button(text='Посчитать', font="14", command=difference)
btn_result.pack(side="bottom", pady=20)

lbl_result = tk.Label(text=f"{square_of_sum} - {sum_of_squares} = {square_of_sum - sum_of_squares}", font="14")
lbl_result.pack(side="bottom", pady=0)

window.mainloop()
