# Написать небольшой скрипт, по типу авторизации пользователя.
# От нас просят логин и пароль.
# Программа должна записать ваш логин и пароль,
# а также время авторизации в текстовый файл


import tkinter as tk
from tkinter import messagebox
import datetime


# Функция для записи информации в файл
def writing_to_file(current_file):
    current_file.write(f"Логин:{ent_login.get()}\n"
                       f"Пароль:{ent_password.get()}\n"
                       f"Дата:{datetime.date.today()}\n"
                       f"Время:{datetime.datetime.now().strftime('%H:%M:%S')}\n\n")


# Функция регистрации пользователя
def registration():
    # Если поля логин и пароль непустые
    if ent_login.get() != '' and ent_password.get() != '':
        with open('users_information.txt', 'r') as file1:
            file1 = file1.read()
            # Проверка на существование логина
            if f"Логин:{ent_login.get()}" in file1:
                messagebox.showinfo('Ошибка', 'Пользователь с таким логином уже зарегистрирован')
            else:
                with open('users_information.txt', 'a') as file2:
                    writing_to_file(file2)
                messagebox.showinfo('Успех', 'Вы зарегистрировались.')
    else:
        messagebox.showinfo('Ошибка', 'Введите логин и пароль.')


# Функция авторизации пользователя
def authorization():
    # Если поля логин и пароль непустые
    if ent_login.get() != '' and ent_password.get() != '':
        with open('users_information.txt', 'r') as file1:
            file1 = file1.read()
            # Проверка логина и пароля
            if f"Логин:{ent_login.get()}\n" \
               f"Пароль:{ent_password.get()}" in file1:
                with open('authorization_information.txt', 'a') as file2:
                    writing_to_file(file2)
                messagebox.showinfo('Успех', 'Вы вошли.')
            else:
                messagebox.showinfo('Ошибка', 'Неправильный логин и/или пароль.')
    else:
        messagebox.showinfo('Ошибка', 'Введите логин и пароль.')


# Функция смены команд и текста на авторизацию
def go_to_authorization():
    lbl_top_text['text'] = 'Вход'
    btn_do['text'] = 'Войти'
    btn_do['command'] = authorization
    lbl_bottom_text['text'] = 'Ещё не зарегистрированы?'
    lbl_bottom_text.place(x=50, y=235)
    btn_transition['text'] = 'Зарегистрироваться'
    btn_transition['command'] = go_to_registration
    btn_transition.place(x=210, y=230)


# функция смены команд и текста на регистрацию
def go_to_registration():
    lbl_top_text['text'] = 'Регистрация'
    btn_do['text'] = 'Зарегистрироваться'
    btn_do['command'] = registration
    lbl_bottom_text['text'] = 'Уже зарегистрированы?'
    lbl_bottom_text.place(x=100, y=235)
    btn_transition['text'] = 'Войти'
    btn_transition['command'] = go_to_authorization
    btn_transition.place(x=250, y=230)


# Создание окна программы
window = tk.Tk()
window['bg'] = 'white'
window.title('Регистрация и авторизация')
window.geometry('400x275+600+150')
window.resizable(False, False)

# Проверка на существование файла users_information.txt
# Если не существует создание его
try:
    open('users_information.txt', 'x').close()
    open('users_information.txt', 'w').close()
    print('Создан файл users_information.txt')
except FileExistsError:
    print('Файл users_information.txt существует.')

# Проверка на существование файла authorization_information.txt
# Если не существует создание его
try:
    open('authorization_information.txt', 'x').close()
    open('authorization_information.txt', 'w').close()
    print('Создан файл authorization_information.txt')
except FileExistsError:
    print('Файл authorization_information.txt существует.')

# Создание текста, кнопок и полей ввода
lbl_top_text = tk.Label(text='Регистрация', font='14', bg='white')
lbl_top_text.pack(side='top', pady=20)

lbl_login = tk.Label(text='Логин: ', font='14', bg='white')
lbl_login.place(x=50, y=90)

lbl_password = tk.Label(text='Пароль: ', font='14', bg='white')
lbl_password.place(x=35, y=135)

ent_login = tk.Entry(width=15, font='14')
ent_login.pack(side='top', pady=20)

ent_password = tk.Entry(width=15, font='14')
ent_password.pack(side='top')

btn_do = tk.Button(
    text='Зарегистрироваться',
    command=registration,
    font='14',
    bg='white')
btn_do.pack(side='top', pady=10)

lbl_bottom_text = tk.Label(
    text='Уже зарегистрированы?',
    bg='white')
lbl_bottom_text.place(x=100, y=235)

btn_transition = tk.Button(
    text='Войти',
    bg='white',
    command=go_to_authorization)
btn_transition.place(x=250, y=230)

window.mainloop()
