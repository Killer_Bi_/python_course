# Создать базу данных рабочих для некого предприятия
# Наполнить таблицу разными способами (Имя, Фамилия, Возраст, Специальность, Образование, Стаж работы, Зарплата)
# Вывести всех рабочих отсортировав по стажу, возрасту, зп
# Вывести первых 5 рабочих
# Удалить любого рабочего

import sqlite3

connect = sqlite3.connect('workers.db')
cursor = connect.cursor()
cursor.execute('''CREATE TABLE workers(name text,
                                       surname text,
                                       age int,
                                       specialization text,
                                       education text,
                                       work_experience int,
                                       salary int)'''
               )
cursor.execute("INSERT INTO workers VALUES('Павел','Романов', 20, 'Токарь', 'Бакалавр', 1, 30000)")
worker = ('Анатолий', 'Сургутин', 30, "Инженер", "Магистр", 2, 50000)
cursor.execute("INSERT INTO workers VALUES(?, ?, ?, ?, ?, ?, ?);", worker)
workers = [('Александр', 'Писарев', 25, "Машинист", "Специалист", 3, 40000),
           ('Роман', 'Толмачёв', 23, "Машинист", "Средний специалист", 4, 35000),
           ('Евгений', 'Савельев', 19, "Менеджер", "Средний специалист", 1, 31000),
           ('Азамат', 'Ложкин', 30, "Разнорабочий", "Общее", 4, 25000)
           ]
cursor.executemany("INSERT INTO workers VALUES(?, ?, ?, ?, ?, ?, ?);", workers)
connect.commit()
for row in cursor.execute('SELECT * FROM workers ORDER BY work_experience'):
    print(row)
print()
for row in cursor.execute('SELECT * FROM workers ORDER BY age'):
    print(row)
print()
for row in cursor.execute('SELECT * FROM workers ORDER BY salary'):
    print(row)
cursor.execute("SELECT * FROM workers")
print("\n", cursor.fetchmany(5), "\n")
cursor.execute("DELETE FROM workers WHERE name='Анатолий';")
connect.commit()
for row in cursor.execute('SELECT * FROM workers'):
    print(row)
