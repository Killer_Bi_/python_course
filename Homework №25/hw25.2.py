# Написать скрипт скачивающий любую картинку

import requests

r = requests.get('https://sun2-10.userapi.com/impg/JDBX6r2S5ZKe_FABSe50-CoYttcsWChDqCRSYA/jVBwHK73M-c.jpg?size=2560x1440&quality=96&sign=c5a7917efbbfec44cecbcd789634d175&type=album')
r.raise_for_status()
with open('img.jpg', 'wb') as file:
    for chunk in r.iter_content(chunk_size=50000):
        file.write(chunk)
