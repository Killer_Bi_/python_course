import datetime
import random as rnd
import tkinter as tk
from tkinter import messagebox


class Item:
    def __init__(self, name, price, quantity=0):
        self.name = name
        self.price = price
        self.quantity = quantity

    def buy_goods(self):
        global money
        if money >= self.price:
            self.quantity += 1
            money -= self.price
        else:
            messagebox.showinfo("Предупреждение",
                                f"У Вас недостаточно средств "
                                f"для покупки товара:\n{self.name}")

    def sell_goods(self):
        global money
        if self.quantity > 0:
            self.quantity -= 1
            money += int(self.price * 1.3)


def buyers():
    global money, list_of_goods
    for goods in list_of_goods:
        if rnd.randint(0, 4) == 1:
            goods.sell_goods()
    window.after(2000, buyers)


def updated_labels():
    global money, date
    global list_of_goods
    global dict_of_goods_lbl
    for goods, lbl in dict_of_goods_lbl.items():
        lbl["text"] = f"{goods.name}\n" \
                      f"Цена: {goods.price}\n" \
                      f"Продажа: {int(goods.price * 1.3)}\n" \
                      f"На складе: {goods.quantity}"
    lbl_money['text'] = f"Деньги: {money}"
    window.after(10, updated_labels)


def updated_date():
    global date, start_date
    global events
    global money, credit
    day_counter = (date - start_date).days
    date += datetime.timedelta(days=1)
    day_counter += 1
    lbl_data["text"] = f"Дата: {date}"
    if day_counter % 15 == 0:
        event = rnd.choice(list(events))
        money += events[event]["Деньги"]
        messagebox.showinfo(event,
                            events[event]["Текст"])
    if day_counter % 30 == 0:
        money -= 30000
        messagebox.showinfo("Аренда",
                            "Оплата аренды в размере 30000 р.")
        if credit > 0:
            credit -= 100000
            money -= 100000
            messagebox.showinfo("Кредит",
                                "Оплата части кредита в размере 100000 р.")
    window.after(5000, updated_date)


def win():
    global money
    if money >= 5000000:
        money -= 5000000
        messagebox.showinfo("Победа!",
                            "Вы смогли отправиться в кругосветное путешествие!")


if __name__ == "__main__":
    # Создание окна игры
    window = tk.Tk()
    window["bg"] = "white"
    window.title('Предприниматель')
    window.geometry('600x440+500+150')
    window.resizable(False, False)

    start_date = datetime.date.today()
    date = start_date
    rent = 30000
    credit = 1000000
    money = credit
    credit = int(credit * 1.2)
    dream_price = 5000000

    events = {"Разовая инвестиция": {"Текст": "Вы получили разовую инвестицию в размере 30000 р.",
                                     "Деньги": 30000
                                     },
              "Выплата от правительства": {"Текст": "Вам поступила выплата от правительства в размере 50000 р.",
                                           "Деньги": 50000
                                           },
              "Налоговая проверка": {"Текст": "Налоговая проверка определила дополнительный налог в размере 25000 р.",
                                     "Деньги": -25000},
              "Пожар в салоне": {"Текст": "Из-за пожара вы потеряли 40000 р.",
                                 "Деньги": -40000},
              "Ограбление": {"Текст": "Вас ограбили. Вы недосчитались 20000 р.",
                             "Деньги": -20000},
              "Наплыв покупателей": {"Текст": "У Вас в магазине наплыв покупателей.",
                                     "Деньги": 20000
                                     },
              "Рекламный бонус": {"Текст": "Вам поступил рекламный бонус.",
                                  "Деньги": 35000
                                  },
              "Быстрая продажа компании": {"Текст": "Одна из компаний прислала вам большой заказ",
                                           "Деньги": 50000
                                           },
              "Случайная порча товара": {"Текст": "Посетитель случайно уронил товар.",
                                         "Деньги": -15000
                                         },
              }

    headphones = Item("Наушники", 1000)
    pocket_game_consoles = Item("Карманные приставки", 10000)
    phones = Item("Телефоны", 25000)
    tv = Item("Телевизоры", 40000)
    computers = Item("Компьютеры", 70000)
    list_of_goods = [headphones,
                     pocket_game_consoles,
                     phones,
                     tv,
                     computers
                     ]

    frm_top = tk.Frame(window, height=10, width=600, bg="white")
    frm_top.pack(fill=tk.X)

    lbl_data = tk.Label(frm_top, text=f"Дата: {date}", bg="white")
    lbl_data.pack(side="right")

    lbl_money = tk.Label(frm_top, text=f"Деньги: {money}", bg="white")
    lbl_money.pack(side="top")

    messagebox.showinfo("Предыстория",
                        "У Вас была мечта. Вы хотели съездить в "
                        "кругосветное путешествие за 5 млн. ₽. "
                        "Вы взяли кредит 1 млн. ₽ под 20% годовых и "
                        "открыли магазин электроники.")

    frm_prices = tk.Frame(window, height=430, width=200, bg="white", border=5)
    frm_prices.pack(side="left")

    dict_of_goods_lbl = {}

    row = 0
    for goods in list_of_goods:
        lbl = tk.Label(frm_prices,
                       text=f"{goods.name}\n"
                            f"Цена: {goods.price}\n"
                            f"Продажа: {goods.price*1.3}\n"
                            f"На складе: {goods.quantity}",
                       bg="white")
        lbl.grid(row=row, column=0)
        dict_of_goods_lbl[goods] = lbl

        btn = tk.Button(frm_prices, text="Купить", command=goods.buy_goods)
        btn.grid(row=row, column=1)

        row += 1

    btn_win = tk.Button(
        window,
        text="Отправится в кругосветное путешествие",
        command=win)
    btn_win.place(x=200, y=370)

    updated_labels()
    updated_date()
    buyers()

    window.mainloop()
