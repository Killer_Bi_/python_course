from django.test import SimpleTestCase, TestCase, Client
from django.urls import reverse
from django.contrib.auth import get_user_model
from .models import GamePost


class GameStoreTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username='testuser',
            email='test@email.com',
            password='secret',
            age=35
        )

        self.post = GamePost.objects.create(
            title='Ведьмак 3: Дикая Охота',
            short_description='«Ведьмак 3: Дикая Охота» — это сюжетная ролевая игра с открытым миром. Её действие разворачивается в поразительной волшебной вселенной, и любое ваше решение может повлечь за собой серьёзные последствия. Вы играете за профессионального охотника на монстров Геральта из Ривии, которому поручено найти Дитя предназначения в огромном мире, полном торговых городов, пиратских островов, опасных горных перевалов и заброшенных пещер.',
            release_date='18 мая. 2015',
            developer='CD PROJEKT RED',
            publisher=self.user,
            genre='Тёмное фэнтези',
            price='1199 pуб.',
        )

    def test_home_status_code(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_correct_template(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')

    def test_string(self):
        post = GamePost(title='Ведьмак 2')
        self.assertEqual(str(post), post.title)

    def test_game_post_content(self):
        self.assertEqual(f'{self.post.title}', 'Ведьмак 3: Дикая Охота')
        self.assertEqual(f'{self.post.short_description}',
                         '«Ведьмак 3: Дикая Охота» — это сюжетная ролевая игра с открытым миром. Её действие разворачивается в поразительной волшебной вселенной, и любое ваше решение может повлечь за собой серьёзные последствия. Вы играете за профессионального охотника на монстров Геральта из Ривии, которому поручено найти Дитя предназначения в огромном мире, полном торговых городов, пиратских островов, опасных горных перевалов и заброшенных пещер.')
        self.assertEqual(f'{self.post.release_date}', '18 мая. 2015')
        self.assertEqual(f'{self.post.developer}', 'CD PROJEKT RED')
        self.assertEqual(f'{self.post.publisher}', 'testuser')
        self.assertEqual(f'{self.post.genre}', 'Тёмное фэнтези')
        self.assertEqual(f'{self.post.price}', '1199 pуб.')

    def test_game_post_list_view(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        # self.assertContains(response,
                            # '«Ведьмак 3: Дикая Охота» — это сюжетная ролевая игра с открытым миром. Её действие разворачивается в поразительной волшебной вселенной, и любое ваше решение может повлечь за собой серьёзные последствия. Вы играете за профессионального охотника на монстров Геральта из Ривии, которому поручено найти Дитя предназначения в огромном мире, полном торговых городов, пиратских островов, опасных горных перевалов и заброшенных пещер.')
        self.assertTemplateUsed(response, 'home.html')

    def test_game_post_detail_view(self):
        response = self.client.get('/game_post/1/')
        no_response = self.client.get('/game_post/99999/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'Ведьмак 3: Дикая Охота')
        self.assertTemplateUsed(response, 'game_post_detail.html')

    def test_get_absolute_url(self):
        self.assertEqual(self.post.get_absolute_url(), '/game_post/1/')

    def test_game_post_create_view(self):
        response = self.client.post(reverse('game_post_new'),
                                    {
                                        'title': 'Ведьмак',
                                        'short_descriptio': '«Ведьмак 3: Дикая Охота» — это сюжетная ролевая игра с открытым миром. Её действие разворачивается в поразительной волшебной вселенной, и любое ваше решение может повлечь за собой серьёзные последствия. Вы играете за профессионального охотника на монстров Геральта из Ривии, которому поручено найти Дитя предназначения в огромном мире, полном торговых городов, пиратских островов, опасных горных перевалов и заброшенных пещер.',
                                        'release_date': '18 мая. 2015',
                                        'developer': 'CD PROJEKT RED',
                                        'publisher': self.user,
                                        'genre': 'Тёмное фэнтези',
                                        'price': '1199 pуб.',
                                    }
                                    )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Ведьмак')
        self.assertContains(response, '1199 pуб.')

    def test_game_post_update_view(self):
        response = self.client.post(reverse('game_post_edit', args='1'),
                                    {
                                        'title': 'Ведьмак 4',
                                        'short_descriptio': '«Ведьмак 3: Дикая Охота» — это сюжетная ролевая игра с открытым миром. Её действие разворачивается в поразительной волшебной вселенной, и любое ваше решение может повлечь за собой серьёзные последствия. Вы играете за профессионального охотника на монстров Геральта из Ривии, которому поручено найти Дитя предназначения в огромном мире, полном торговых городов, пиратских островов, опасных горных перевалов и заброшенных пещер.',
                                        'release_date': '18 мая. 2015',
                                        'developer': 'CD PROJEKT RED',
                                        'genre': 'Тёмное фэнтези',
                                        'price': '1199 pуб.',
                                    }
                                    )
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Ведьмак 4')

    def test_game_post_delete_view(self):
        response = self.client.get(reverse('game_post_delete', args='1'))
        self.assertEqual(response.status_code, 200)

class SignUpPageTest(TestCase):
    username = 'newuser'
    email = 'newuser@mail.com'

    def test_signup_status_code(self):
        response = self.client.get('/users/signup')
        self.assertEqual(response.status_code, 301)

    def test_view_by_name(self):
        response = self.client.get(reverse('signup'))
        self.assertEqual(response.status_code, 200)

    def test_correct_template(self):
        response = self.client.get(reverse('signup'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'signup.html')

    def test_signup_form(self):
        new_user = get_user_model().objects.create_user(self.username, self.email)
        self.assertEqual(get_user_model().objects.all().count(), 1)
        self.assertEqual(get_user_model().objects.all()[0].username, self.username)
        self.assertEqual(get_user_model().objects.all()[0].email, self.email)
