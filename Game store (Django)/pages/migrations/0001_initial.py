# Generated by Django 3.2.9 on 2021-12-14 11:14

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='GamePost',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=150, verbose_name='Название игры')),
                ('short_description', models.TextField(verbose_name='Краткое описание')),
                ('release_date', models.CharField(max_length=150, verbose_name='Дата выхода')),
                ('developer', models.CharField(max_length=150, verbose_name='Разработчик')),
                ('genre', models.CharField(max_length=150, verbose_name='Жанр')),
                ('price', models.CharField(max_length=150, verbose_name='Цена')),
                ('publisher', models.ForeignKey(max_length=150, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Издатель')),
            ],
            options={
                'verbose_name': 'Игра',
                'verbose_name_plural': 'Игры',
            },
        ),
    ]
