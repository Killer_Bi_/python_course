from rest_framework import serializers
from .models import GamePost


class GamePostSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id',
                  'title',
                  'developer',
                  'publisher',
                  'price',
                  'date'
                  )
        model = GamePost
