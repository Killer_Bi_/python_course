from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.HomePageView.as_view(), name='home'),
    path('game_post/<int:pk>/', views.GameStoreDetailView.as_view(), name='game_post_detail'),
    path('game_post/new', views.GameStoreCreateView.as_view(), name='game_post_new'),
    path('game_post/<int:pk>/edit/', views.GameStoreUpdateView.as_view(), name='game_post_edit'),
    path('game_post/<int:pk>/delete/', views.GameStoreDeleteView.as_view(), name='game_post_delete'),
    path('game_post/<int:pk>/new', views.ReviewCreateView.as_view(), name='review_new'),
    path('game_post/<int:pk>/<int:pk2>/edit/', views.ReviewUpdateView.as_view(), name='review_edit'),
    path('game_post/<int:pk>/<int:pk2>/delete/', views.ReviewDeleteView.as_view(), name='review_delete'),
    path('api/v1/', views.GamePostList.as_view(), name='game_post_list_api'),
    path('api/v1/<int:pk>/', views.GamePostDetail.as_view(), name='game_post_detail_api'),
    path('api/v1/rest-auth/', include('rest_auth.urls')),
    path('api/v1/rest-auth/registration', include('rest_auth.registration.urls')),
]
