from django.views.generic import ListView, DetailView
from . import models
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework import generics, permissions
from .serializers import GamePostSerializer
from .permissions import IsAuthorOrReadOnly


class HomePageView(ListView):
    model = models.GamePost
    template_name = 'home.html'


class GameStoreDetailView(DetailView):
    model = models.GamePost
    template_name = 'game_post/game_post_detail.html'
    context_object_name = 'post'


class GameStoreCreateView(LoginRequiredMixin, CreateView):
    model = models.GamePost
    template_name = 'game_post/game_post_new.html'
    fields = ['title',
              'image',
              'short_description',
              'release_date',
              'developer',
              'genre',
              'price']
    login_url = 'login'

    def form_valid(self, form):
        form.instance.publisher = self.request.user
        return super().form_valid(form)


class GameStoreUpdateView(LoginRequiredMixin, UpdateView):
    model = models.GamePost
    template_name = 'game_post/game_post_edit.html'
    fields = ['title',
              'image',
              'short_description',
              'release_date',
              'developer',
              'genre',
              'price']
    login_url = 'login'


class GameStoreDeleteView(LoginRequiredMixin, DeleteView):
    model = models.GamePost
    template_name = 'game_post/game_post_delete.html'
    success_url = reverse_lazy('home')
    context_object_name = 'post'
    login_url = 'login'


class ReviewCreateView(LoginRequiredMixin, CreateView):
    model = models.Review
    template_name = 'review/review_new.html'
    fields = ['review']
    login_url = 'login'

    def form_valid(self, form):
        form.instance.author = self.request.user
        # Нужно сделать нормально
        form.instance.game_post = models.GamePost.objects.get(pk=self.request.path[-5])
        return super().form_valid(form)


class ReviewUpdateView(LoginRequiredMixin, UpdateView):
    model = models.Review
    template_name = 'review/review_edit.html'
    fields = ['review']
    login_url = 'login'

    def form_valid(self, form):
        form.instance.author = self.request.user
        # Нужно сделать нормально
        form.instance.game_post = models.GamePost.objects.get(pk=self.request.path[-7])
        return super().form_valid(form)


class ReviewDeleteView(LoginRequiredMixin, DeleteView):
    model = models.Review
    template_name = 'review/review_delete.html'
    success_url = reverse_lazy('home')
    login_url = 'login'

class GamePostList(generics.ListCreateAPIView):
    queryset = models.GamePost.objects.all()
    serializer_class = GamePostSerializer

class GamePostDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes =(IsAuthorOrReadOnly,)
    queryset = models.GamePost.objects.all()
    serializer_class = GamePostSerializer