from django.db import models
from django.urls import reverse

class GamePost(models.Model):
    date = models.DateField(auto_now_add=True, verbose_name='Дата создания')
    title = models.CharField(max_length=150, verbose_name='Название игры')
    image = models.ImageField(upload_to='images/', verbose_name='Обложка')
    short_description = models.TextField(verbose_name='Краткое описание')
    release_date = models.CharField(max_length=150, verbose_name='Дата выхода')
    developer = models.CharField(max_length=150, verbose_name='Разработчик')
    publisher = models.ForeignKey('users.CustomUser',
                                  max_length=150,
                                  on_delete=models.CASCADE,
                                  verbose_name='Издатель')
    genre = models.CharField(max_length=150, verbose_name='Жанр')
    price = models.CharField(max_length=150, verbose_name='Цена')

    class Meta:
        verbose_name = 'Игра'
        verbose_name_plural = 'Игры'

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('game_post_detail', args=[str(self.id)])

    def delete(self, *args, **kwargs):
        storage, path = self.image.storage, self.image.path
        super(GamePost, self).delete(*args, **kwargs)
        storage.delete(path)


class Review(models.Model):
    date = models.DateField(auto_now_add=True, verbose_name='Дата создания')
    game_post = models.ForeignKey(GamePost,
                                  related_name='reviews',
                                  on_delete=models.CASCADE,
                                  verbose_name='Игра')
    review = models.CharField(max_length=100, verbose_name='Отзыв')
    author = models.ForeignKey('users.CustomUser', on_delete=models.CASCADE, verbose_name='Автор')

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'

    def __str__(self):
        return self.review

    def get_absolute_url(self):
        return reverse('game_post_detail', args=[str(self.game_post.id)])
