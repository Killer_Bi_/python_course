from django.contrib import admin
from . import models


class ReviewInLine(admin.StackedInline):
    model = models.Review


@admin.register(models.GamePost)
class GamePostAdmin(admin.ModelAdmin):
    inlines = [
        ReviewInLine,
    ]
    list_display = ('title', 'date', 'release_date', 'developer', 'publisher', 'price')
    list_filter = ('date', 'developer', 'publisher')
    search_fields = ('title', 'developer')
    date_hierarchy = 'date'
    order_by = ('title', 'release_date')


admin.site.register(models.Review)
