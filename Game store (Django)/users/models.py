from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings


class CustomUser(AbstractUser):
    age = models.PositiveIntegerField(default=0, verbose_name='Возраст')

class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE,
                                verbose_name='Пользователь',)
    date_birth = models.DateTimeField(blank=True,
                                      null=True,
                                      verbose_name='Дата рождения',)
    photo = models.ImageField(upload_to='photos/',
                              blank=True,
                              verbose_name='Фото'
    )

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'

    def __str__(self):
        return f"Профиль пользователя: {self.user.username}"
