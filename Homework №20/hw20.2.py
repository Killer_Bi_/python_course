# Вернуть домены из списка email-адресов

import re


list_of_emails = ["iosif.polodashvili@mail.ru", "polodashvilii@gmail.com", "polodashvili@yandex.ru"]
p = re.compile(r"(?P<name>[0-9a-zA-Z._-]+)@(?P<mail>[a-z0-9-]+)[.](?P<domain>[a-z]{2,6})")
for email in list_of_emails:
    print(p.search(email).group("domain"), end=" ")
