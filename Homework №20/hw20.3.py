# Извлечь дату из произвольной строки

import re


p = re.compile(r"[0-3][0-9].[01][0-9].[12][0-9]{3}")
print(p.search(input()).group())
