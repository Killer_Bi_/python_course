# На вход подается строка из символов(пример: aAb bBc2ca!b aa!sxdc)
# Необходимо найти самое длинное слово(словом является последовательность буквенных символов.Если
# встречается любой символ не являющийся буквой, то слово обрывается)
# После этого требуется сдвинуть всю строку на количество символов, соответсвующее длине самого длинного
# слова, не затрагивая символы не являющиеся буквами.
# Учитывать регистр символов! (пример вывода: eEf fFg2ge!f ee!wbhg)


import re
from string import ascii_uppercase, ascii_lowercase


string = input("Введите строку: ")

length = len(max(re.split(r"\W|\d", string), key=len))
list_of_chars = list(string)

for i in range(len(list_of_chars)):
    char = list_of_chars[i]
    if char.isalpha():
        if char.islower():
            list_of_chars[i] = ascii_lowercase[(ascii_lowercase.index(char) + length) % 26]
        else:
            list_of_chars[i] = ascii_uppercase[(ascii_uppercase.index(char) + length) % 26]

print(''.join(list_of_chars))
