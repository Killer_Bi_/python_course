# Написать программу которая принимает строку слов.
# Находит самое длинное слово(слово - последовательность символов,
# состоящее из букв латинского алфавита. Если встречается символ
# не являющийся буквой, то слово на данном месте обрыватся)
# и записать в переменную length. После это сдвинуть все символы
# строки на length символов согласно английского алфавита.


import re
from string import ascii_uppercase, ascii_lowercase


string = input("Введите строку: ")

length = len(max(re.split(r"\W|\d", string), key=len))
list_of_chars = list(string)

for i in range(len(list_of_chars)):
    char = list_of_chars[i]
    if char.isalpha():
        if char.islower():
            list_of_chars[i] = ascii_lowercase[(ascii_lowercase.index(char) + length) % 26]
        else:
            list_of_chars[i] = ascii_uppercase[(ascii_uppercase.index(char) + length) % 26]

print(''.join(list_of_chars))
