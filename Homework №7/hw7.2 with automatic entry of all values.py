for number_of_tomcats in range(1, 2001):
    remainder = number_of_tomcats % 10

    if 10 <= number_of_tomcats % 100 <= 20:
        print(f"{number_of_tomcats} котов")
    elif remainder == 1:
        print(f"{number_of_tomcats} кот")
    elif 2 <= remainder <= 4:
        print(f"{number_of_tomcats} кота")
    elif 5 <= remainder <= 9 or remainder == 0:
        print(f"{number_of_tomcats} котов")
