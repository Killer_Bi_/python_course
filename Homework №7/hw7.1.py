name = input("Введите Ваше имя: ")
surname = input("Введите Вашу фамилию: ")
age = input("Введите Ваш возраст: ")

if name.istitle() and name.isalpha() and name[1:].islower():
    print("Имя верно.")
else:
    print("Неверное имя.\n"
          "Имя должно состоять только из букв и начинаться с большой буквы.")

if surname.istitle() and surname.isalpha() and surname[1:].islower():
    print("Фамилия верна.")
else:
    print("Неверная фамилия.\n"
          "Фамилия должна состоять только из букв и начинаться с большой буквы.")

if age.isdigit() and int(age) >= 0:
    print("Возраст верный.")
else:
    print("Неверный возраст.\n"
          "Возраст должен состоять только из цифр и не может быть отрицательным")
