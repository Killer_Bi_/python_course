# Написать программу на свободную тему с количеством методов
# не меньше 3 и прописать необходимые тесты.

def del_char(str_, char):
    return str_.replace(char, "")


def char_to_upper(str_, char):
    return str_.replace(char, char.upper())


def char_to_lower(str_, char):
    return str_.replace(char, char.lower())
