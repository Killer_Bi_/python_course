import unittest
import hw23_3


class TextChangerTest(unittest.TestCase):
    def test_result_del_char(self):
        self.assertEqual(hw23_3.del_char(
                                         "Не совершай классическую ошибку всех умников: "
                                         "не думай, что нет людей умнее тебя.",
                                         "е"
                                         ),
                         "Н совршай классичскую ошибку всх умников: "
                         "н думай, что нт людй умн тбя."
                         )

    def test_type_result_del_char(self):
        self.assertIs(type(hw23_3.del_char(
                                           "Не совершай классическую ошибку всех умников: "
                                           "не думай, что нет людей умнее тебя.",
                                           "е"
                                           )), str)

    def test_result_char_to_upper(self):
        self.assertEqual(hw23_3.char_to_upper(
                                              "Не совершай классическую ошибку всех умников: "
                                              "не думай, что нет людей умнее тебя.",
                                              "е"
                                              ),
                         "НЕ совЕршай классичЕскую ошибку всЕх умников: "
                         "нЕ думай, что нЕт людЕй умнЕЕ тЕбя."
                         )

    def test_type_result_char_to_upper(self):
        self.assertIs(type(hw23_3.char_to_upper(
                                                "Не совершай классическую ошибку всех умников: "
                                                "не думай, что нет людей умнее тебя.",
                                                "е"
                                                )
                           ), str)

    def test_result_char_to_lower(self):
        self.assertEqual(hw23_3.char_to_lower(
                                              "Не совершай классическую ошибку всех умников: "
                                              "не думай, что нет людей умнее тебя.",
                                              "Н"
                                              ),
                         "не совершай классическую ошибку всех умников: "
                         "не думай, что нет людей умнее тебя."
                         )

    def test_type_result_char_to_lower(self):
        self.assertIs(type(hw23_3.char_to_lower(
                                                "Не совершай классическую ошибку всех умников: "
                                                "не думай, что нет людей умнее тебя.",
                                                "Н"
                                                )
                           ), str)


if __name__ == "__main__":
    unittest.main()
