import unittest
import hw23_2


class PalindromTest(unittest.TestCase):
    def test_result(self):
        self.assertEqual(hw23_2.palindrom(1187164175119312638), 8362139115714617811)

    def test_type_result(self):
        self.assertIs(type(hw23_2.palindrom(1187164175119312638)), int)


if __name__ == "__main__":
    unittest.main()
