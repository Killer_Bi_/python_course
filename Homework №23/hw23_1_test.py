import unittest
import hw23_1


class FactorialTest(unittest.TestCase):
    def test_result(self):
        self.assertEqual(hw23_1.factorial(10), 3628800)

    def test_type_result(self):
        self.assertIs(type(hw23_1.factorial(10)), int)


if __name__ == "__main__":
    unittest.main()
