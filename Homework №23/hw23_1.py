# Написать программу для нахождения факториала
# и выполнить все необходимые тесты(как пример: тип, значение)


def factorial(n):
    if n == 1:
        return 1
    else:
        return n * factorial(n - 1)

print(factorial(10))