# Написать метод/функцию, который/которая на вход принимает массив городов.
# В качестве результата возвращает строку, где города разделены запятыми, а в конце стоит точка.
# Пример:
# «Москва, Санкт-Петербург, Воронеж.»


def commas_and_dot(list_of_cities):
    return ", ".join(list_of_cities) + "."


print(commas_and_dot(["Москва", "Санкт-Петербург", "Воронеж"]))
