# Написать консольную программу используя классы.Выбор программы на ваше усмотрение.
# Как вариант можно написать консольную игру по типу поединок.
# Важно! Должно использоваться наследование

import pyfiglet
import time


class Character:
    def __init__(self):
        self.hp = 100
        self.dmg = 10
        self.armor = 0
        self.name = "Персонаж"

    def attack(self, opponent):

        if opponent.armor >= self.dmg:
            print(f"{opponent.name} заблокировал весь урон от {self.name}\n")
        else:
            opponent.hp -= self.dmg - opponent.armor
            print(f"{opponent.name} получил {self.dmg - opponent.armor} урона от {self.name}\n"
                  f"Здоровье {opponent.name}: {opponent.hp}")


class Hero(Character):
    def __init__(self):
        super().__init__()
        self.name = "Герой"
        self.money = 0

    def get_reward(self, opponent):
        self.money += opponent.value


class Goblin(Character):
    def __init__(self):
        super().__init__()
        self.hp = 30
        self.dmg = 3
        self.name = "Гоблин"
        self.value = 10


class Orc(Character):
    def __init__(self):
        super().__init__()
        self.hp = 50
        self.dmg = 10
        self.name = "Орк"
        self.value = 50


class Chimera(Character):
    def __init__(self):
        super().__init__()
        self.hp = 100
        self.dmg = 20
        self.name = "Химера"
        self.value = 150


if __name__ == "__main__":
    print(pyfiglet.figlet_format("Arena Hero", font="calgphy2"))
    time.sleep(3)

    goblin_1 = Goblin()
    orc_1 = Orc()
    goblin_2 = Goblin()
    orc_2 = Orc()
    chimera = Chimera()

    battle_counter = 1
    enemy_counter = 0
    enemies_list = [goblin_1, goblin_2, orc_1, orc_2, chimera]
    hero = Hero()
    hero.name = input("Введите ваше имя:")
    print(f"Приветствую тебя, {hero.name}! Ты один из многих гладиаторов этой арены.\n")

    while True:
        if enemy_counter == len(enemies_list):
            print("\nВы победили всех врагов и получили титул \"Герой арены!\"")
            break
        this_enemy = enemies_list[enemy_counter]
        print(f"Это твой {battle_counter} бой\n"
              f"Твой противник {this_enemy.name}.")
        time.sleep(1)
        input(f"\nГотов, {hero.name}? ")
        time.sleep(1)
        print("Пошёл!\n")
        while hero.hp > 0 and this_enemy.hp > 0:
            time.sleep(1)
            hero.attack(this_enemy)
            if this_enemy.hp < 1:
                hero.get_reward(this_enemy)
                print(f"\nВы победили {this_enemy.name} и получили {this_enemy.value} золотых.\n"
                      f"У тебя {hero.money} золотых.")
                break
            time.sleep(1)
            this_enemy.attack(hero)
            if hero.hp < 1:
                print(f"\nВы проиграли...")
                break
        while True:
            print("Добро пожаловать в мою скромную лавку!\n"
                  "Здесь вы можете купить: \n"
                  "(1) Улучшить меч +4 к атаке. Цена: 20 золотых.\n"
                  "(2) Улучшить броню +3 к броне. Цена: 25 золотых.\n"
                  "(3) Улучшить щит +2 к броне. Цена: 15 золотых.\n"
                  "(4) Уйти")
            choose = input("Чего изволите? ")
            if choose == "1" and hero.money >= 20:
                hero.dmg += 4
                hero.money -= 20
                print(f"Вы улучшили меч. Ваш урон {hero.dmg}."
                      f"Ваше золото {hero.money}.")
            elif choose == "2" and hero.money >= 25:
                hero.armor += 3
                hero.money -= 25
                print(f"Вы улучшили броню. Ваша защита {hero.armor}."
                      f"Ваше золото {hero.money}.")
            elif choose == "3" and hero.money >= 15:
                hero.armor += 2
                hero.money -= 15
                print(f"Вы улучшили щит. Ваша защита {hero.armor}."
                      f"Ваше золото {hero.money}.")
            elif choose == "4":
                break
            else:
                print("У вас недостаточно денег или неправильный выбор.")
            time.sleep(1)
        enemy_counter += 1
        battle_counter += 1
