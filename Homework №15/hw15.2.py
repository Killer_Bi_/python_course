# Написать метод/функцию, который/которая на вход принимает число (float),
# а на выходе получает число, округленное до пятерок.
# Пример:
# 27 => 25, 27.8 => 30, 41.7 => 40.


def round_five(float_num):
    if float_num % 5 >= 2.5:
        return int(float_num // 5 * 5 + 5)
    else:
        return int(float_num // 5 * 5)


print(round_five(27), round_five(27.8), round_five(41.7))
