# Напишите программу для поиска URL-адресов в строке.

import re


str_ = "Мороз и солнце,https://www.youtube.com/feed/subscriptions " \
       "деньhttps://www.youtube.com чудесный!" \
       "https://www.vk.com/video?q=%D0%BD%D0%B0%D1%80%D1%83%D1%82%D0%BE%201%20%D1%81%D0%B5%D0%B7%D0%BE%D0%BD%20205"
p = re.compile(r"https://www\.[a-z]+\.[a-z]{2,6}[/[a-zA-Z0-9%&?=]+]*")
print(*p.findall(str_))
