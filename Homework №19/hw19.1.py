# 1. Дана строка python best language. Проверить с
# помощью регулярных выражений найти с какого по
# какой символ встречается слово python

import re

text = "python best language"
pattern = "python"
match = re.search(pattern, text)
print(match.start(), match.end())
