# 3. Дана произвольная строка. Найти и вывести все слова,
# после которых идет запятая и пробел(если данные слова присутствуют)

import re

text = input()

pattern = re.compile(r"\w+(?=[,][ ])", re.S | re.I)
print(pattern.findall(text))
