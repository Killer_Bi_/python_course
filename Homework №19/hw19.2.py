# 2. Дана произвольная строка.
# Проверить встречается ли в ней дата время формата чч.мм.сс.

import re

text = input()
pattern = re.compile(r"[0-3]\d.[01]\d\.\d\d")
print("Дата найдена." if pattern.search(text) else "Дата не найдена.")
