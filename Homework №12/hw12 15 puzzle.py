import time
import datetime as dt
import tkinter as tk
from tkinter import messagebox
from random import shuffle


# Закрытие окна игры
def exit_the_game():
    window.destroy()


# Функция замены текста времени каждые 100 миллисекунд
def tick():
    global stopwatch
    stopwatch = window.after(100, tick)
    t = dt.datetime.fromtimestamp(int(round(time.time() - zero_time, 0))).strftime("%M:%S")
    lbl_time['text'] = f"Время: {t}"


# Функция остановки замены текста времени каждые 100 миллисекунд
def stop_tick():
    window.after_cancel(stopwatch)


# Вывод сообщения с правилами
def rules():
    messagebox.showinfo("Правила",
                        "Расположите костяшки так, чтобы "
                        "числа шли подряд слева направо и сверху вниз.")


# Вывод сообщения с информацией о программе
def info():
    messagebox.showinfo("О программе",
                        "Автор: Полодашвили Иосиф\n"
                        "Версия: 1.2.1\n"
                        "Дата: 23.09.2021")


# Установка размера игры 3x3
def n_3():
    stop_tick()
    global n
    n = 3
    messagebox.showinfo("Предупреждение",
                        "При игре 3x3 не все комбинации могут быть решаемыми.\n"
                        "Этот режим находится в разработке, приношу свои извинения.")
    window.geometry('300x340')
    new_game(n)


# Установка размера игры 4x4
def n_4():
    global n
    stop_tick()
    n = 4
    window.geometry('400x440')
    new_game(n)


# Установка размера игры 5x5
def n_5():
    global n
    stop_tick()
    n = 5
    messagebox.showinfo("Предупреждение",
                        "При игре 5x5 не все комбинации могут быть решаемыми.\n"
                        "Этот режим находится в разработке, приношу свои извинения.")
    window.geometry('500x540')
    new_game(n)


# Создание новой комбинации кнопок
def new_game(size_of_game):
    global turn, list_of_buttons, winning_combination, is_first_click
    # Запуск и остановка секундомера
    tick()
    stop_tick()
    #
    lbl_time['text'] = "Время: 00:00"
    # Установка значения переменной первый клик
    # Чтобы с первого клика игры пошёл отсчёт
    is_first_click = True
    print("Новая игра")
    # Обнуление счётчика ходов и надписи с количеством ходов
    turn = 0
    lbl_turn["text"] = f"{turn} ход"
    # Установка выигрышной комбинации
    winning_combination = list(range(1, size_of_game ** 2)) + [""]
    # Создание счётчика для проверки на решаемость
    counter = size_of_game
    # Уничтожение существующих кнопок
    for x in range(len(list_of_buttons)):
        for y in range(len(list_of_buttons)):
            list_of_buttons[x][y].destroy()
    # Очистка списка кнопок
    list_of_buttons.clear()
    while True:
        # Перемешивание значений которые будут записаны в кнопки
        random_values = winning_combination[:-1]
        shuffle(random_values)
        # Проверка на решаемость если игра 4x4
        if size_of_game == 4:
            for i in range(len(random_values)):
                for j in range(len(random_values[i + 1:])):
                    if random_values[i] > random_values[i + 1 + j]:
                        counter += 1
            if counter % 2 == 0:
                random_values += ['']
                break
            else:
                counter = size_of_game
        else:
            break
    # Объявление вспомогательной переменной
    i = 0
    # Создание и установка положений кнопок
    for x in range(size_of_game):
        list_of_buttons.append([])
        for y in range(size_of_game):
            # Создание кнопок
            if i + 1 == size_of_game ** 2:
                button = tk.Button(window, text='', width=13, height=6, relief=tk.SUNKEN, bg="#cccccc")
            else:
                button = tk.Button(window, text='', width=13, height=6, relief=tk.RAISED)
            # добавление кнопок в список с кнопками
            list_of_buttons[x].append(button)
            # Установка положения кнопок
            list_of_buttons[x][y].place(x=y * 100, y=40 + x * 100)
            # Установка функции кнопок при клике
            list_of_buttons[x][y]['command'] = lambda selected_button=button: clicked(selected_button)
            # Установка значений кнопок
            if i + 1 != size_of_game ** 2:
                list_of_buttons[x][y]['text'] = random_values[i]
                i += 1
    # Вызов функции темы
    dark_theme()


# Функция перемещения кнопок при нажатии
def clicked(btn):
    global turn, list_of_buttons, winning_combination, n, is_first_click, zero_time
    if is_first_click:
        zero_time = time.time()
        tick()
        is_first_click = False
    # Объявление списка для проверки на выигрыш
    list_of_values = []
    # Объявление переменной для проверки изменения количества ходов
    this_turn = turn
    # Объявление переменных индексов пустой кнопки
    x_empty = y_empty = 0
    # Установка переменных индексов пустой кнопки
    for x in range(len(list_of_buttons)):
        for y in range(len(list_of_buttons[0])):
            if list_of_buttons[x][y]['text'] == '':
                x_empty, y_empty = x, y
    # Перемещение кнопок
    for x in range(len(list_of_buttons)):
        for y in range(len(list_of_buttons[0])):
            # Если нажатая кнопка это текущая кнопка списка
            if list_of_buttons[x][y] == btn:
                # Пустая и нажатая кнопки меняются местами если
                # они находятся на расстоянии одной клетки друг от друга
                # по горизонтали или вертикали
                if abs(x_empty - x) + abs(y_empty - y) == 1:
                    list_of_buttons[x][y], list_of_buttons[x_empty][y_empty] = \
                        list_of_buttons[x_empty][y_empty], list_of_buttons[x][y]
                    turn += 1
                    break
                # "Длинный ход" если пустая кнопка снизу
                # от кнопки на которую нажали
                elif x < x_empty and y == y_empty:
                    for k in range(x_empty - 1, x - 1, -1):
                        clicked(list_of_buttons[k][y])
                    turn += 1 - len(range(x_empty - 1, x - 1, -1))
                    break
                # "Длинный ход" если пустая кнопка сверху
                # от кнопки на которую нажали
                elif x > x_empty and y == y_empty:
                    for k in range(x_empty + 1, x + 1):
                        clicked(list_of_buttons[k][y])
                    turn += 1 - len(range(x_empty + 1, x + 1))
                    break
                # "Длинный ход" если пустая кнопка справа
                # от кнопки на которую нажали
                elif y < y_empty and x == x_empty:
                    for k in range(y_empty - 1, y - 1, -1):
                        clicked(list_of_buttons[x][k])
                    turn += 1 - len(range(y_empty - 1, y - 1, -1))
                    break
                # "Длинный ход" если пустая кнопка слева
                # от кнопки на которую нажали
                elif y > y_empty and x == x_empty:
                    for k in range(y_empty + 1, y + 1):
                        clicked(list_of_buttons[x][k])
                    turn += 1 - len(range(y_empty + 1, y + 1))
                    break
        # изменение текста с количеством ходов если оно изменилось
        if this_turn != turn:
            lbl_turn["text"] = f"{turn} ход"
            print(f'Ход {turn}')
            break
    for x in range(len(list_of_buttons)):
        for y in range(len(list_of_buttons[0])):
            # Добавление в список значения кнопок
            list_of_values.append(list_of_buttons[x][y]['text'])
            # Установка положения кнопок
            list_of_buttons[x][y].place(x=y * 100, y=40 + x * 100)
    # Проверка на выигрыш
    if list_of_values == winning_combination:
        messagebox.showinfo("Поздравляем", f"Вы победили!\n"
                                           f"{lbl_time['text']}\n"
                                           f"Количество ходов: {turn}")
        print("Победа!")
        # Остановка секундомера
        stop_tick()
        new_game(n)


# Функция установки тёмной темы оформления
def dark_theme():
    # Если флажок в меню стоит
    if is_dark.get():
        # Фон окна устанавливается на тёмно-серый
        window['bg'] = '#454545'
        frm_top['bg'] = window['bg']
        # Фон надписи с количеством ходов
        # устанавливается на тёмно-серый,
        # а надпись на белый
        lbl_turn['bg'] = window['bg']
        lbl_turn['fg'] = 'white'
        # Тоже самое для надписи времени
        lbl_time['bg'] = window['bg']
        lbl_time['fg'] = lbl_turn['fg']
    # Если флажок в меню не стоит
    else:
        # Фон окна устанавливается на белый
        window['bg'] = 'white'
        frm_top['bg'] = window['bg']
        # Фон надписи с количеством ходов
        # устанавливается на белый,
        # а надпись на чёрный
        lbl_turn['bg'] = window['bg']
        lbl_turn['fg'] = 'black'
        # Тоже самое для надписи времени
        lbl_time['bg'] = window['bg']
        lbl_time['fg'] = lbl_turn['fg']
    # Цикл установки цвета кнопок в зависимости
    # от флажка тёмной темы в меню
    for x in range(len(list_of_buttons)):
        for y in range(len(list_of_buttons[0])):
            if is_dark.get():
                if list_of_buttons[x][y]['text'] != '':
                    list_of_buttons[x][y]['bg'] = "#454545"
                    list_of_buttons[x][y]['fg'] = "white"
                else:
                    list_of_buttons[x][y]['bg'] = "white"
            else:
                if list_of_buttons[x][y]['text'] != '':
                    list_of_buttons[x][y]['bg'] = "white"
                    list_of_buttons[x][y]['fg'] = "black"
                else:
                    list_of_buttons[x][y]['bg'] = "#cccccc"


# Создание окна игры
window = tk.Tk()
window.title('Пятнашки')
window.geometry('400x440+600+150')
window.resizable(False, False)
# Объявление основных переменных:
# Список кнопок
list_of_buttons = []
# Количество ходов
turn = 0
# Размер игры
n = 4
# Переменная для разности между ней и текущим временем
zero_time = 0
# Переменная для проверки на первое нажатие кнопки в игре
is_first_click = True
# Переменная для функции секундомера
stopwatch = 0
# Список для выигрышной комбинации
winning_combination = []
# Создание переменной галочки "Тёмная тема"
is_dark = tk.BooleanVar()
# Создание области для надписей ходов и времени
frm_top = tk.Frame(window, height=20, bg='black')
# Установка области на верх окна игры
frm_top.pack(side=tk.TOP, fill=tk.X)
# Создание надписи с количеством ходов
lbl_turn = tk.Label(frm_top, text=f"{turn} ход")
# Установка надписи на верх области
lbl_turn.pack(side=tk.TOP)
# Создание надписи с временем игры
lbl_time = tk.Label(frm_top, text=f"Время: 00:00")
# Установка надписи вниз области
lbl_time.pack(side=tk.BOTTOM)
# Создание меню и применение его к окну игры
main_menu = tk.Menu(window)
window.config(menu=main_menu)
# Создание меню "Меню"
game_menu = tk.Menu(main_menu, tearoff=0)
# Создание пункта меню "Новая игра"
game_menu1 = tk.Menu(game_menu, tearoff=0)
# Добавление в пункт "Новая игра" подпунктов
game_menu1.add_command(label='3x3', command=n_3)
game_menu1.add_command(label='4x4', command=n_4)
game_menu1.add_command(label='5x5', command=n_5)
# Добавление пункта "Новая игра" в меню "Меню"
game_menu.add_cascade(label='Новая игра', menu=game_menu1)
# Добавление галочки "Тёмная тема"
game_menu.add_checkbutton(
    label='Тёмная тема',
    variable=is_dark,
    command=dark_theme)
# Создание меню "Справка"
help_menu = tk.Menu(main_menu, tearoff=0)
# Добавление пунктов в меню "Справка"
help_menu.add_command(label='Правила', command=rules)
help_menu.add_command(label='О программе', command=info)
# Добавление пунктов меню
main_menu.add_cascade(label='Меню', menu=game_menu)
main_menu.add_cascade(label='Справка', menu=help_menu)
main_menu.add_cascade(label='Выход', command=exit_the_game)
# Создание новой игры 4x4
new_game(4)

window.mainloop()
