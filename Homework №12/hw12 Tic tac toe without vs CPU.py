import tkinter as tk
from tkinter import messagebox


# Вывод сообщения с правилами
def rules():
    messagebox.showinfo("Правила",
                        "Игроки по очереди ставят на свободные клетки "
                        "поля 3×3 знаки (один всегда крестики, другой всегда нолики). "
                        "Первый, выстроивший в ряд 3 своих фигуры по вертикали, "
                        "горизонтали или диагонали, выигрывает. Первый ход делает игрок, "
                        "ставящий крестики.")


# Вывод сообщения с информацией о программе
def info():
    messagebox.showinfo("О программе",
                        "Автор: Полодашвили Иосиф\n"
                        "Версия: 1.0\n"
                        "Дата: 23.09.2021")


# Функция установки тёмной темы оформления
def dark_theme():
    # Если флажок в меню стоит
    if is_dark.get():
        # Фон окна устанавливается на тёмно-серый
        window['bg'] = '#454545'
        # Фон надписи с количеством побед
        # устанавливается на тёмно-серый,
        # а надпись на белый
        lbl_wins['bg'] = window['bg']
        lbl_wins['fg'] = 'white'
    else:
        # Фон окна устанавливается на белый
        window['bg'] = 'white'
        # Фон надписи с количеством побед
        # устанавливается на белый,
        # а надпись на чёрный
        lbl_wins['bg'] = window['bg']
        lbl_wins['fg'] = 'black'
    for x in range(len(list_of_buttons)):
        for y in range(len(list_of_buttons[0])):
            if is_dark.get():
                list_of_buttons[x][y]['bg'] = "#454545"
                list_of_buttons[x][y]['fg'] = "white"
            else:
                list_of_buttons[x][y]['bg'] = "white"
                list_of_buttons[x][y]['fg'] = "black"


# Функция для определения победителя
def whose_victory(char):
    global Cross_wins, Zero_wins
    if char == 'X':
        Cross_wins += 1
        print("Победа X")
    else:
        Zero_wins += 1
        print("Победа O")
    lbl_wins['text'] = f"X: {Cross_wins} O: {Zero_wins}"


# Функция присвоения текста кнопкам при нажатии
# и проверки на победу и ничью
def clicked(btn):
    global turn
    global list_of_buttons
    if btn['text'] == '':
        if turn % 2 == 0:
            btn['text'] = 'X'
            turn += 1
        else:
            btn['text'] = 'O'
            turn += 1
        print(f"{turn} Ход")
    # Проверка на диагональную(\) победу
    if list_of_buttons[0][0]['text'] ==\
            list_of_buttons[1][1]['text'] ==\
            list_of_buttons[2][2]['text'] and\
            list_of_buttons[0][0]['text'] != '':
        whose_victory(list_of_buttons[0][0]['text'])
        tk.messagebox.showinfo("Конец игры", f"Победили {list_of_buttons[0][0]['text']}")
        new_game()
    # Проверка на диагональную(/) победу
    elif list_of_buttons[0][2]['text'] ==\
            list_of_buttons[1][1]['text'] ==\
            list_of_buttons[2][0]['text'] and\
            list_of_buttons[0][2]['text'] != '':
        whose_victory(list_of_buttons[2][0]['text'])
        tk.messagebox.showinfo("Конец игры", f"Победили {list_of_buttons[2][0]['text']}")
        new_game()
    # Проверки на горизонтальные победы победы
    for x in range(3):
        if list_of_buttons[x][0]['text'] ==\
                list_of_buttons[x][1]['text'] ==\
                list_of_buttons[x][2]['text'] and\
                list_of_buttons[x][2]['text'] != '':
            whose_victory(list_of_buttons[x][0]['text'])
            tk.messagebox.showinfo("Конец игры", f"Победили {list_of_buttons[x][0]['text']}")
            new_game()
    # Проверки на вертикальные победы победы
    for y in range(3):
        if list_of_buttons[0][y]['text'] ==\
                list_of_buttons[1][y]['text'] ==\
                list_of_buttons[2][y]['text'] and\
                list_of_buttons[0][y]['text'] != '':
            whose_victory(list_of_buttons[0][y]['text'])
            tk.messagebox.showinfo("Конец игры", f"Победили {list_of_buttons[0][y]['text']}")
            new_game()
    # Проверки на ничью
    if turn == 9:
        tk.messagebox.showinfo("Конец игры", "Ничья")
        print("Ничья")
        new_game()


# Создание новой комбинации кнопок
def new_game():
    global list_of_buttons, turn
    print("Новая игра")
    # Обнуление счётчика ходов
    turn = 0
    if list_of_buttons == []:
        for x in range(3):
            list_of_buttons.append([])
            for y in range(3):
                # Создание кнопки
                button = tk.Button(window, width=13, height=6)
                # добавление кнопок в список с кнопками
                list_of_buttons[x].append(button)
                # Установка функции кнопок при клике
                list_of_buttons[x][y]['command'] = lambda selected_button=button: clicked(selected_button)
                # Установка положения кнопок
                button.place(x=y * 100, y=20 + x * 100)
    else:
        for butn_list in list_of_buttons:
            for butn in butn_list:
                butn['text'] = ''


# Создание окна игры
window = tk.Tk()
window.title('Крестики-нолики')
window.geometry('300x320+600+150')
window.resizable(False, False)
# Список кнопок
list_of_buttons = []
# Количество ходов
turn = 0
# Переменная побед крестиков
Cross_wins = 0
# Переменная побед ноликов
Zero_wins = 0
# Создание переменной галочки "Тёмная тема"
is_dark = tk.BooleanVar()
# Создание надписи с количеством побед
lbl_wins = tk.Label(window, text=f"X: {Cross_wins} O: {Zero_wins}")
# Установка надписи на верх окна
lbl_wins.pack(side=tk.TOP)
# Создание меню и применение его к окну игры
main_menu = tk.Menu(window)
window.config(menu=main_menu)
# Создание меню "Меню"
game_menu = tk.Menu(main_menu, tearoff=0)
# Добавление пункта "Новая игра" в меню "Меню"
game_menu.add_command(label='Новая игра', command=new_game)
# Добавление галочки "Тёмная тема"
game_menu.add_checkbutton(
    label='Тёмная тема',
    variable=is_dark,
    command=dark_theme)
# Создание меню "Справка"
help_menu = tk.Menu(main_menu, tearoff=0)
# Добавление пунктов в меню "Справка"
help_menu.add_command(label='Правила', command=rules)
help_menu.add_command(label='О программе', command=info)
# Добавление пунктов меню
main_menu.add_cascade(label='Меню', menu=game_menu)
main_menu.add_cascade(label='Справка', menu=help_menu)
main_menu.add_cascade(label='Выход', command=window.destroy)
# Создание новой игры
new_game()

window.mainloop()
