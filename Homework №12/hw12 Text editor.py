import tkinter as tk
from tkinter import scrolledtext, messagebox, filedialog


def change_wrap_type():
    if wrap_type.get() == 'Перенос по словам':
        scr_text['wrap'] = tk.WORD
    elif wrap_type.get() == 'Перенос по символам':
        scr_text['wrap'] = tk.CHAR
    elif wrap_type.get() == 'Без переноса':
        scr_text['wrap'] = tk.NONE


# Функция установки тёмной темы оформления
def dark_theme():
    # Если флажок в меню стоит
    if is_dark.get():
        # Фон окна устанавливается на тёмно-серый
        window['bg'] = '#454545'
        # Фон области ввода текста
        # устанавливается на тёмно-серый,
        # а текста на белый.
        scr_text['bg'] = window['bg']
        scr_text['fg'] = 'white'
    else:
        # Фон окна устанавливается на белый
        window['bg'] = 'white'
        # Фон области ввода текста
        # устанавливается на белый,
        # а текста на чёрный
        scr_text['bg'] = window['bg']
        scr_text['fg'] = 'black'


# функция открытия файла
def open_file():
    global file_path
    file_name = ''
    file_path = filedialog.askopenfilename(filetypes=(("Текстовые файлы", "*.txt"),))
    for i in range(len(file_path)):
        if file_path[-(i + 1)] == '/':
            file_name = file_path[-i:]
            break
    window.title(file_name + " — Текстовый редактор")
    scr_text.delete(1.0, tk.END)
    with open(f"{file_path}", encoding='UTF-8') as file:
        scr_text.insert(1.0, file.read())


# функция сохранения файла
def save_file():
    global file_path
    with open(f"{file_path}", 'w', encoding='UTF-8') as file:
        file.write(scr_text.get(1.0, tk.END))


# функция сохранения файла как
def save_file_as():
    with filedialog.asksaveasfile(filetypes=(("Текстовые файлы", "*.txt"),), defaultextension='.txt') as file:
        file.write(scr_text.get(1.0, tk.END))


#
def create_file():
    scr_text.delete(1.0, tk.END)


# Вывод сообщения с информацией о программе
def info():
    messagebox.showinfo("О программе",
                        "Автор: Полодашвили Иосиф\n"
                        "Версия: 1.0\n"
                        "Дата: 24.09.2021")


# Создание текстового редактора
window = tk.Tk()
window.title('Текстовый редактор')
window.geometry('400x440+600+150')

# Создание переменной галочки "Тёмная тема"
is_dark = tk.BooleanVar()
# Создание переменной для меню "Формат"
wrap_type = tk.StringVar()
#
wrap_type.set('Без переноса')

# Переменная пути к файлу
file_path = ''

# Создание поля ввода текста
scr_text = tk.scrolledtext.ScrolledText(window, wrap=tk.NONE)
scr_text.focus()
scr_text.pack(expand=True, fill=tk.BOTH)

# Создание меню и применение его к окну игры
main_menu = tk.Menu()
window.config(menu=main_menu)
# Создание меню "Файл"
file_menu = tk.Menu(main_menu, tearoff=0)
# Создание пунктов в меню "Файл"
file_menu.add_command(label='Создать', command=create_file)
file_menu.add_command(label='Открыть...', command=open_file)
file_menu.add_command(label='Сохранить', command=save_file)
file_menu.add_command(label='Сохранить как...', command=save_file_as)
file_menu.add_separator()
file_menu.add_command(label='Выход', command=window.destroy)
# Создание меню "Формат"
format_menu = tk.Menu(main_menu, tearoff=0)
# Добавление пунктов в меню "Формат"
format_menu.add_radiobutton(
    label='Перенос по словам',
    value='Перенос по словам',
    variable=wrap_type,
    command=change_wrap_type)
format_menu.add_radiobutton(
    label='Перенос по символам',
    value='Перенос по символам',
    variable=wrap_type,
    command=change_wrap_type)
format_menu.add_radiobutton(
    label='Без переноса',
    value='Без переноса',
    variable=wrap_type,
    command=change_wrap_type)
# Создание меню "Справка"
help_menu = tk.Menu(main_menu, tearoff=0)
# Добавление пунктов в меню "Справка"
help_menu.add_command(label='О программе', command=info)
# Создание меню "Вид"
view_menu = tk.Menu(main_menu, tearoff=0)
# Добавление пунктов в меню "Вид"
# Добавление галочки "Тёмная тема"
view_menu.add_checkbutton(
    label='Тёмная тема',
    variable=is_dark,
    command=dark_theme)
# Добавление пунктов меню
main_menu.add_cascade(label='Файл', menu=file_menu)
main_menu.add_cascade(label='Формат', menu=format_menu)
main_menu.add_cascade(label='Вид', menu=view_menu)
main_menu.add_cascade(label='Справка', menu=help_menu)

window.mainloop()
