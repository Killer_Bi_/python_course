# Написать программу,
# которая принимает два списка чисел и выводит
# все элементы из первого списка, которых нет во втором.


list1 = input().split()
list2 = input().split()
print(*[num for num in list1 if num not in list2])
