import tkinter as tk


def not_in_second():
    list_1 = ent_string1.get().split()
    list_2 = ent_string2.get().split()
    result = [str(num) for num in list_1 if num not in list_2]
    lbl_result2["text"] = " ".join(result)


window = tk.Tk()
window.title("11 Задача 2")

frm_1 = tk.Frame(master=window, width=600, height=450)
frm_1.pack(fill=tk.X)

frm_2 = tk.Frame(master=window, width=600, height=450)
frm_2.pack(fill=tk.X, ipadx=5, ipady=5)

lbl_string1 = tk.Label(master=frm_1, text="Введите строку:")
lbl_string1.grid(row=0, column=0)

ent_string1 = tk.Entry(master=frm_1, width=50)
ent_string1.grid(row=0, column=1)

lbl_string2 = tk.Label(master=frm_1, text="Введите строку:")
lbl_string2.grid(row=1, column=0)

ent_string2 = tk.Entry(master=frm_1, width=50)
ent_string2.grid(row=1, column=1)

lbl_result1 = tk.Label(master=frm_1, text="Результат:")
lbl_result1.grid(row=2, column=0)

lbl_result2 = tk.Label(master=frm_1)
lbl_result2.grid(row=2, column=1)

btn_result = tk.Button(
    master=frm_2,
    text="Выполнить",
    width=10,
    command=not_in_second)
btn_result.pack(fill=tk.X)

window.mainloop()
