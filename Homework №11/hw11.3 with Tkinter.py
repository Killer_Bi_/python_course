import tkinter as tk


def how_much_words():
    list_ = ent_string.get().split()
    lbl_result2["text"] = len(list_)


window = tk.Tk()
window.title("11 Задача 3")

frm_1 = tk.Frame(master=window, width=600, height=450)
frm_1.pack(fill=tk.X)

frm_2 = tk.Frame(master=window, width=600, height=450)
frm_2.pack(fill=tk.X, ipadx=5, ipady=5)

lbl_string = tk.Label(master=frm_1, text="Введите строку:")
lbl_string.grid(row=0, column=0)

ent_string = tk.Entry(master=frm_1, width=50)
ent_string.grid(row=0, column=1)

lbl_result1 = tk.Label(master=frm_1, text="Количество слов:")
lbl_result1.grid(row=2, column=0)

lbl_result2 = tk.Label(master=frm_1)
lbl_result2.grid(row=2, column=1)

btn_result = tk.Button(
    master=frm_2,
    text="Выполнить",
    width=10,
    command=how_much_words)
btn_result.pack(fill=tk.X)

window.mainloop()
