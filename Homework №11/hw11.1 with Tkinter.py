import tkinter as tk


def remove_and_pow():
    list_ = [int(num) for num in ent_string.get().split()]
    i = len(list_) - 1
    while i > 0:
        if i % 2 == 0:
            del list_[i]
        else:
            list_[i] **= 2
        i -= 1
    del list_[0]
    list_ = [str(num) for num in list_]
    lbl_result2["text"] = " ".join(list_)


window = tk.Tk()
window.title("11 Задача 1")

frm_1 = tk.Frame(master=window, width=600, height=450)
frm_1.pack(fill=tk.X)

frm_2 = tk.Frame(master=window, width=600, height=450)
frm_2.pack(fill=tk.X, ipadx=5, ipady=5)

lbl_string = tk.Label(master=frm_1, text="Введите строку:")
lbl_string.grid(row=0, column=0)

ent_string = tk.Entry(master=frm_1, width=50)
ent_string.grid(row=0, column=1)

lbl_result1 = tk.Label(master=frm_1, text="Результат:")
lbl_result1.grid(row=1, column=0)

lbl_result2 = tk.Label(master=frm_1)
lbl_result2.grid(row=1, column=1)

btn_result = tk.Button(
    master=frm_2,
    text="Выполнить",
    width=10,
    command=remove_and_pow)
btn_result.pack(fill=tk.X)

window.mainloop()
