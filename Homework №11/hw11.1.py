# На вход программе подается список чисел. Требуется написать функцию,
# которая удаляет каждый второй элемент из списка(начиная с первого элемента),
# а остальные элементы возводит во вторую степень.
# Функция не должна ничего возвращать.


def remove_and_pow(list_):

    i = len(list_) - 1
    while i > 0:
        if i % 2 == 0:
            del list_[i]
        else:
            list_[i] **= 2
        i -= 1
    del list_[0]


list_of_numbers = [int(num) for num in input().split()]
remove_and_pow(list_of_numbers)
print(*list_of_numbers)
