# Простые делители числа 13195 - это 5, 7, 13 и 29.
# Написать программу, которая находит самый большой
# делитель числа 600851475143, являющийся простым числом?


def list_of_prime_numbers(number):
    list_of_numbers = [num for num in range(2, number + 1)]
    list_of_prime = []
    for i in range(0, number + 1):
        if i < len(list_of_numbers):
            list_of_prime.append(list_of_numbers[i])
            list_of_numbers = [1] + [num for num in list_of_numbers if num % list_of_numbers[i] != 0]
        else:
            break
    return list_of_prime


number = 600851475143
prime_list = list_of_prime_numbers(number//2 +1)
for i in prime_list[::-1]:
    if number % i == 0:
        print(i)
        break
