# Найти произведение каждого 3 числа
# последовательности Фибонначи, которые не превышают 2 миллиона.


def list_fibonacci(fib):
    if fib[-1] + fib[-2] > 2000000:
        return fib
    fib.append(fib[-1] + fib[-2])
    list_fibonacci(fib)
    return fib


result = 1
fibonacci_series = list_fibonacci([1, 2])
for num in fibonacci_series[::3]:
    result *= num
print(result)
