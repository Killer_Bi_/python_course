number = input("Введите число: ")
is_palindrome = True

for i in range(0, len(number)//2):
    if number[i] != number[-i-1]:
        is_palindrome = False
        break

if is_palindrome:
    print("Число является полиндромом.")
else:
    print("Число не является полиндромом.")
