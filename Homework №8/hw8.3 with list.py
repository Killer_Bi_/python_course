number = 1
list_of_numbers = []
result = 0

while number != 0:
    number = int(input("Введите число(0 для выхода): "))
    if number != 0:
        list_of_numbers.append(number)

if len(list_of_numbers) != 0:
    result = 1

for number in list_of_numbers:
    result *= number**2

print(f"Результат: {result}")
