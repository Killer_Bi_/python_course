import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

con = psycopg2.connect(user='postgres', password='1234', host='127.0.0.1')  # , database='student_db')

cur = con.cursor()
con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
cur.execute('create database student_db')
cur.close()
con.close()
